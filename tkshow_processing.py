# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["TkShowProcess", "show_sequence"]

import sys
import itertools as it
import collections
import queue
import typing as tp
#import enum
#import tkinter as tk
import numpy as np
from PIL import Image

from .base import ControlResponse
from .base import TKSHOW_EVENT, SimpleQueue
from .base import TKSHOW_FEATURE
from .image import GraphicsCmdList
from .control import TkshowControl
from .control import TksignalControl


img_types = (Image.Image, np.ndarray)


def is_iterable(maybe_iterable) -> bool:
    try:
        iter(maybe_iterable)
    except TypeError:
        return False
    return True


class IterWrap(object):

    __callable : tp.Callable[[int],tp.Any]
    __min_idx  : int
    __max_idx  : int
    __cur_idx  : int

    def __init__(self, callabl : tp.Callable[[int],tp.Any], min_idx : int, max_idx : int):
        self.__callable = callabl
        self.__min_idx = min_idx
        self.__max_idx = max_idx
        self.__cur_idx = min_idx
    
    def __iter__(self):
        return self

    def __next__(self):
        if self.__cur_idx < self.__max_idx:
            val = self.__callable(self.__cur_idx)
            self.__cur_idx += 1
            return val
        return None # calls StopIteration automatically

    def __len__(self):
        return (self.__max_idx - self.__min_idx)

    def __call__(self, idx : int):
        return self.__callable(self.__min_idx + idx)
    #def __getitem__(self, idx : int):
    #    return self(idx)



class TkShowProcess(ControlResponse):

    run                : tp.Callable[[],tp.NoReturn]

    __signal_views     : dict
    __image_views      : dict


    def __init__(self):
        super(TkShowProcess,self).__init__()

        # processor initialized with calling processor
        self.run = self.process_callables

        self.__signal_views = dict()
        self.__image_views = dict()
    

    def add_image_view(self, seq : tp.Union[tp.Iterable,tp.Sequence,tp.Callable[[int],tuple]], 
                         max_idx : tp.Optional[int] = None,
                         min_idx : int = 0,
                        view_key : tp.Optional[str] = None ):
        
        if view_key is None:
            view_key = self.__create_key( "image" )
        
        if max_idx is None:
            max_idx = sys.maxsize
        max_idx = int(max_idx)
        min_idx = int(min_idx)


        if callable(seq) or isinstance( seq, collections.abc.Sequence ):

            if isinstance( seq, collections.abc.Sequence ):
                max_idx = min(max_idx, len(seq) - 1)

                # store widget for later usage, convert sequence into callable
                self.__image_views[view_key] = IterWrap(lambda idx: seq[min_idx+idx], 0, max_idx-min_idx)
            else:
                # store widget for later usage
                self.__image_views[view_key] = IterWrap(seq, min_idx, max_idx)

        elif is_iterable(seq):
            # store widget for later usage
            self.__image_views[view_key] = seq
            
            # only iterator processing is possible
            self.run = self.process_iterators
        else:
            raise TypeError( "Only sequences and iterables are supported (%r provided)" % repr(seq) )
        
        return view_key


    def add_signal_view(self, num_axes=1, view_key : tp.Optional[str] = None ):
        if view_key is None:
            view_key = self.__create_key( "signal" )

        signal_control = TksignalControl( num_axes = int(num_axes), control_response = self )
        self.__signal_views[view_key] = signal_control
        return (view_key, signal_control)


    def __create_key( self, prefix : str ):

        for idx in it.count( len(self.__signal_views) + len(self.__image_views) ):
            key = "%s_%d" % (prefix, idx)
            if (key not in self.__signal_views) and (key not in self.__image_views):
                break
        return key


    @staticmethod
    def dispatch_iterator_items( imgIter : tp.Iterable):
        """Dispatches items in iterator stream.
           * Images are returned with "yield"
           * GCLs are concatenated and returned with yield
        """
        next_image = None

        # loop until first image
        for item in imgIter:
            if isinstance( item, img_types ):
                next_image = item
                break
               
        # initialize graphics command list
        gcl = list()

        for item in imgIter:
            if isinstance( item, GraphicsCmdList ):
                gcl.append( item )
                continue
            elif not isinstance( item, img_types ):
                item, gcl_cmd = item
                assert isinstance( gcl, GraphicsCmdList )
                gcl.append( gcl_cmd )
            
            if isinstance( item, img_types ):
                n_gcl = GraphicsCmdList()
                n_gcl.directives = sum( [x.directives for x in gcl], [])
                yield (next_image, n_gcl)
                next_image = item
                gcl = list() # initialize graphics command list
            else:
                raise ValueError("Unexpected type %r" % type(item) )


    def process_iterators(self):

        # get all image sources
        img_iters = tuple( map( self.dispatch_iterator_items, self.__image_views.values() ) )

        # create windows
        kwargs = dict( features = TKSHOW_FEATURE.NEXT_IMAGE | TKSHOW_FEATURE.FORWARD_START_STOP, control_response = self )
        show_controls = tuple( TkshowControl(**kwargs) for img_iter in img_iters )


        mode = TKSHOW_EVENT.NEXT_IMAGE
        mode_value = None
        skip_counter = 0

        for cur_idx, images in enumerate( zip( *img_iters ) ):
            #for image, gcl in self.dispatch_iterator_items( imgIter ):

            while skip_counter > 0:
                # skip some frames
                skip_counter -= 1
                continue

            if mode in {TKSHOW_EVENT.CLOSE_WINDOW, TKSHOW_EVENT.EXIT}:
                break

            any_is_active = any(show_control.is_active() for show_control in show_controls)
            if not any_is_active:
                break

            if mode in {TKSHOW_EVENT.FORWARD_START_STOP, TKSHOW_EVENT.NEXT_IMAGE}:
                # update all signal plots
                for signal_control in self.__signal_views:
                    if signal_control.is_active():
                        signal_control.set_xpos( cur_idx )
                
                # update all image viewers
                for show_control, (image,gcl) in zip(show_controls,images):
                    if show_control.is_active():
                        show_control.set_image( image, gcl )
            else:
                assert isinstance( mode, TKSHOW_EVENT )
            
            # Get mode in next run
            if mode is TKSHOW_EVENT.FORWARD_START_STOP:
                try:
                    n_mode,mode_value = self.event_queue.get(block=False)
                    if n_mode is TKSHOW_EVENT.FORWARD_START_STOP:
                        n_mode = TKSHOW_EVENT.NEXT_IMAGE
                except queue.Empty:
                    pass
            else:
                n_mode,mode_value = self.event_queue.get(block=True)

            # handle jumps, if ordered
            if n_mode is TKSHOW_EVENT.JUMP_TO:
                skip_counter = int(mode_value) - cur_idx
                n_mode = mode

            mode = n_mode

    # ---------------------------------------------------------------------------------------------------------
    class TkShowPresenter(object):

        img_controls    : tuple
        img_calls       : tuple
        signal_controls : tuple


        @property
        def is_active(self) -> bool:
            return any(img_control.is_active() for img_control in self.img_controls)


        def __init__(self, img_controls : tuple,
                            img_calls : tuple,
                        signal_controls : tuple ):
            self.img_controls = img_controls
            self.img_calls = img_calls
            self.signal_controls = signal_controls


        def next(self, next_idx : int):
            # update all signal plots
            for signal_control in self.signal_controls:
                if signal_control.is_active():
                    signal_control.set_xpos( next_idx )

            # gather next set of images
            images = ( img_call(next_idx) for img_call in self.img_calls )
            
            # storing inactive images
            non_active_set = set()

            # set new images
            for img_control, image in zip(self.img_controls,images):
                if img_control.is_active():
                    if not isinstance( image, img_types ):
                        image, gcl = image
                        assert isinstance( gcl, GraphicsCmdList )
                        img_control.set_image( image, gcl )
                    else:
                        img_control.set_image( image )
                else:
                    # idx became inactive
                    non_active_set.add(img_control)

            # checking which windows are still active
            if non_active_set:
                if len(non_active_set) == len(self.img_controls):
                    self.img_controls = tuple()
                    self.img_calls = tuple()
                else:
                    self.img_controls, self.img_calls = \
                        tuple(zip(*tuple( (s,c) for s,c in zip(self.img_controls,self.img_calls) if s not in non_active_set )))


    def process_callables(self):

        # get all image sources
        img_calls = tuple( self.__image_views.values() )
        max_idx = min(map(len,img_calls))

        # create windows
        kwargs = dict( features = TKSHOW_FEATURE.NEXT_IMAGE | TKSHOW_FEATURE.FORWARD_START_STOP | \
                                  TKSHOW_FEATURE.PREV_IMAGE | TKSHOW_FEATURE.BACKWARD_START_STOP,
               control_response = self )
        
        presenter = self.TkShowPresenter( img_controls = tuple( TkshowControl(**kwargs) for _ in img_calls ),
                                             img_calls = img_calls,
                                       signal_controls = tuple(self.__signal_views.values()) )
        
        min_idx = 0
        assert min_idx <= max_idx

        cur_idx = -1
        next_idx = 0
        mode = TKSHOW_EVENT.NONE
        mode_value = None

        while presenter.is_active:

            # See if we can show an image or have to quit
            if mode in {TKSHOW_EVENT.CLOSE_WINDOW, TKSHOW_EVENT.EXIT}:
                break
            
            # Get data at next index und display it
            if cur_idx != next_idx:
                presenter.next( next_idx )
                cur_idx = next_idx

            # query for next event: blocking in single step mode und non-blocking in FORWARD/BACKWARD mode
            if mode in {TKSHOW_EVENT.FORWARD_START_STOP, TKSHOW_EVENT.BACKWARD_START_STOP}:
                try:
                    n_mode,mode_value = self.event_queue.get(block=False)
                    if mode == n_mode:
                        n_mode = TKSHOW_EVENT.NONE
                except queue.Empty:
                    pass
            else:
                n_mode,mode_value = self.event_queue.get(block=True)
            

            # handle jumps, if ordered
            if n_mode is TKSHOW_EVENT.JUMP_TO:
                next_idx = max( min_idx, min( int(mode_value), max_idx ) )
                n_mode = mode
            else:
                # No jumps
                mode = n_mode

                # progress image indices
                if mode in {TKSHOW_EVENT.NEXT_IMAGE, TKSHOW_EVENT.FORWARD_START_STOP}:
                    # count forward
                    next_idx = cur_idx + 1
                    if next_idx > max_idx:
                        next_idx = min_idx
                elif mode in {TKSHOW_EVENT.PREVIOUS_IMAGE, TKSHOW_EVENT.BACKWARD_START_STOP}:
                    # count backward
                    next_idx = cur_idx - 1
                    if next_idx < min_idx:
                        next_idx = max_idx
    # ---------------------------------------------------------------------------------------------------------

# ------------------------------------------------------------------------------


def show_sequence( seq : tp.Union[tp.Iterable,tp.Sequence], 
               max_idx : tp.Optional[int] = None,
               min_idx : int = 0 ):

    tkp = TkShowProcess()
    tkp.add_image_view( seq, max_idx=max_idx, min_idx=min_idx )
    tkp.run()

