# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["TkSignalCursor"]

import typing as tp
import numpy as np


class TkSignalCursor(object):

    # Constants:
    # percentage above and below plots
    HEIGHT_MARGIN_FACTOR : float = 0.05

    artist        : tp.Any
    y_value_plots : list
    x_value_plot  : tp.Optional[tp.Any]
    

    @property
    def ax(self):
        return self.artist.axes


    def __init__(self, ax, 
                       x0 : float,
                     ymin : float,
                     ymax : float,
            enable_xvalue : bool):
        
        # Extend y-range with margin
        ymargin = (ymax - ymin) * self.HEIGHT_MARGIN_FACTOR
        ymin = ymin - ymargin
        ymax = ymax + ymargin
        self.artist = ax.plot( (x0,x0), (ymin,ymax), color = 'green', linewidth=0.75, alpha = 0.6, animated=True )[0]
        self.y_value_plots = list()
        
        if bool(enable_xvalue):
            self.x_value_plot = self.__create_value_plot(verticalalignment="bottom", horizontalalignment = "center", fontsize = "small")
            self.x_value_plot.set_y(ymax)
        else:
            self.x_value_plot = None

        ax.set_ylim((ymin,ymax))
    

    def set_visible(self, val : bool):
        self.artist.set_visible(val)

        if self.x_value_plot is not None:
            self.x_value_plot.set_visible(val)

        for value in self.y_value_plots:
            value.set_visible( val )


    def set_invisible(self):
        self.set_visible(False)


    def set_pos(self, xpos : float, ypos : tp.Optional[np.ndarray] = None):
        self.artist.set_xdata(xpos)

        if self.x_value_plot is not None:
            self.x_value_plot.set_x(xpos)
            self.x_value_plot.set_text(self.__number2string(xpos))

        if ypos is not None:
            ypos = np.ma.asarray(ypos)
            assert ypos.ndim == 1
            assert ypos.size >= len(self.y_value_plots)

            while len(self.y_value_plots) < ypos.size:
                self.y_value_plots.append( self.__create_value_plot(verticalalignment="top") )

            for y_plot, yval in zip(self.y_value_plots,ypos):
                
                if np.ma.is_masked(yval):
                    # ignore invalid values
                    y_plot.set_text( "" )
                else:
                    y_plot.set_x(xpos)
                    y_plot.set_y(yval)
                    y_plot.set_text( "  " + self.__number2string(yval))


    def draw(self):
        self.set_visible(True)
        # re-render the artist, updating the canvas state, but not the screen
        self.ax.draw_artist( self.artist )

        if self.x_value_plot is not None:
            self.ax.draw_artist( self.x_value_plot )
        
        for y_plot in self.y_value_plots:
            self.ax.draw_artist( y_plot )
    

    @staticmethod
    def __number2string( num : float ):
        anum = abs(num)
        if (anum >= 1e4) or (anum < 1e-4):
            return ("%.2e" % num)
        if anum > 0:
            return ("%0.2f" % num)
        return ("%.4f" % num)


    def __create_value_plot(self, verticalalignment = "top",
                                horizontalalignment = "left",
                                           fontsize = "x-small"):
        return self.ax.text( -100, -100, "  val", 
                        animated = True,
                           color = 'green',
               verticalalignment = verticalalignment,
             horizontalalignment = horizontalalignment,
                        fontsize = fontsize,
                           alpha = 0.6 )

