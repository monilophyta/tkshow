# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["TkSignal", "open_tksignal_window"]


#import sys
import threading
import typing as tp
import tkinter as tk
import collections

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot
matplotlib.pyplot.style.use('dark_background')
import matplotlib.colors as mcolors

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

#from matplotlib.axes import Axes
from matplotlib.lines import Line2D

import numpy as np

from ..base import call_from_tk, tk_main_thread, tk_root
from ..base import TkshowABC
from ..base import ControlResponse
from ..base import SimpleQueue, TKSHOW_EVENT

from .tkshow_signal_cursor import TkSignalCursor


SIGNAL_COLORS = list( mcolors.TABLEAU_COLORS.keys() )



class TkSignal( FigureCanvasTkAgg, TkshowABC ):

    figure               : Figure
    axes                 : tuple
    cursors              : list
    X                    : np.ndarray   # [ T ]
    Ys                   : np.ndarray   # [ A x { Ls : T } ]

    fig_buffer           : tp.Optional[tp.Any]


    @property
    def canvas(self) -> tk.Canvas:
        return self.get_tk_widget()

    @property
    def master(self):
        return self.canvas.master
    
    @property
    def after_idle(self):
        return self.canvas.after_idle


    def __init__(self, master, 
                     num_axes : int,
             control_response : ControlResponse, **argv ):

        assert num_axes > 0
        self.figure = Figure(figsize=(16,num_axes), tight_layout=True)
        FigureCanvasTkAgg.__init__(self, self.figure, master=master, **argv)

        # initalize Tkshow base class
        TkshowABC.__init__(self, control_response=control_response)

        self.__bind_to_signals()

        self.master.protocol("WM_DELETE_WINDOW", self.do_close )
        self.master.bind('<Escape>', self.do_close )
        self.master.bind("<Configure>", self.__on_resize)

        # bind do mouse activity
        mouse_event_id = self.figure.canvas.mpl_connect('button_press_event', self.__on_mouse_button )

        self.__create_figure( max( 1, num_axes ) )
        
        self.fig_buffer = None

        # trigger activity
        self._set_active()


    def __create_figure(self, num_axes : int ):

        axes = self.figure.subplots( nrows = num_axes,
                                     ncols = 1,
                                    sharex = True, 
                                    sharey = False,
                                   squeeze = False,
                                subplot_kw = {},
                               gridspec_kw = {} )
        self.axes = tuple(axes[:,0])
        self.cursors = list()


    def __bind_to_signals(self):
        self.master.bind("<F5>", self.control_response.do_forward)
        self.master.bind("<space>", self.control_response.do_forward)

        self.master.bind("<F6>", self.control_response.do_backward)

        self.master.bind("<Right>", self.control_response.next_image)
        self.master.bind("<Left>", self.control_response.previous_image)



    def plot( self, X : np.ndarray, *Ys ):
        assert isinstance( X, (collections.abc.Sequence, np.ndarray) )
        
        X = np.asarray(X)
        xlim = (X.min(),X.max())
        self.X = X   # [ T ]
        self.Ys = tuple( np.ma.asarray( list(Y.values()) ) for Y in Ys ) # [ A x { Ls : T } ]

        # Plot timestamp above upper axis
        enable_xvalue = True

        for ax, Y in zip( self.axes, Ys ):
            if len(Y) == 0:
                continue         
            assert isinstance( Y, dict ), "Invalid Type %r" % type(Y)
            Y = Y.items()
            ymin = np.inf
            ymax = -np.inf

            ax.clear()
            ax.grid(True, linestyle=":", color="darkgreen")

            for idx, (label, y) in enumerate(Y):
                #print(y)
                ax.plot( X, y, linewidth=1.0, color=SIGNAL_COLORS[idx%len(SIGNAL_COLORS)], alpha=0.7, label=label )

                if (not np.ma.isMaskedArray(y)) or (not np.all(y.mask)):
                    ymin = min( ymin, y.min() )
                    ymax = max( ymax, y.max() )
            

            # create cursor
            self.cursors.append( TkSignalCursor(ax, X[0], ymin, ymax, enable_xvalue=enable_xvalue) )
            enable_xvalue = False  # no further timestamp on other axis

            ax.set_xlim(xlim)
            # Remove the plot frame lines. They are unnecessary here.
            ax.spines['top'].set_visible(False)
            #ax.spines['bottom'].set_color('gray')
            ax.spines['bottom'].set_visible(False)
            #ax.spines['bottom'].set_alpha( 0.5 )
            ax.spines['right'].set_visible(False)
            ax.spines['left'].set_color('darkgreen')
            ax.spines['left'].set_alpha( 0.5 )
            ax.tick_params(axis='both', labelsize="small", colors='darkgreen')

            # create legend
            if len(Y) > 2:
                legend_fontsize = "x-small"
                num_cols = (len(Y) + 2) // 3
            else:
                legend_fontsize = "small"
                num_cols = 1
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=num_cols, fontsize=legend_fontsize, frameon=False)

            # set legend
            #ax.legend()

        self.canvas.pack(expand = True, fill=tk.BOTH)

        # cache drawn background
        self.__cache_signal_view()
        
        # draw cursor after caching background
        self.__draw_cursor()
    

    def set_xpos(self, xidx : int):

        # reset the background back in the canvas state, screen unchanged
        if self.fig_buffer is not None:
            self.restore_region( self.fig_buffer )

            xpos = self.X[xidx]
            for cursor, Y in zip(self.cursors, self.Ys):
                cursor.set_pos(xpos, Y[:,xidx])

            # draw updated cursor
            self.__draw_cursor()

    # -------------------------------------------------------------------------------------

    def __on_resize(self, event):
        cur_size = tuple(self.get_width_height())
        new_size = (event.width, event.height)
        
        if cur_size != new_size:
            # ignore when resize has not been consolidated yet
            pass
        elif (self.fig_buffer is not None) and (cur_size != tuple(self.fig_buffer.get_extents()[2:])):
            #self.master.configure(width=event.width, height=event.height)
            self.__cache_signal_view()
            self.__draw_cursor()

    # -------------------------------------------------------------------------------------

    def __cache_signal_view(self):
        """Disables current cursors, replots(redraws) and stores it in a background buffer"""

        # make cursor invisble
        for cursor in self.cursors:
            cursor.set_visible(False)

        # trigger drawing
        self.figure.canvas.draw()

        # get copy of entire figure (everything inside fig.bbox) sans the cursor
        self.fig_buffer = self.copy_from_bbox(self.figure.bbox)
    

    def __draw_cursor(self):
        """Sets cursor visiable and draws them on canvas"""
        
        for cursor in self.cursors:
            #cursor.set_visible(True)
            cursor.draw()

        # copy the image to the GUI state, but screen might not be changed yet
        self.blit(self.figure.bbox)
        
        # flush any pending GUI events, re-painting the screen if needed
        self.flush_events()


    def __on_mouse_button(self, event):
        if (not event.dblclick) and (event.button == 2) and (event.xdata is not None):
            # Only react on middle button and within the plots
            xidx = np.searchsorted(self.X, event.xdata, side="left")
            self.control_response.do_jump( xidx )


# -------------------------------------------------------------------------------------------------


def open_tksignal_window( *args, **kwargs ) -> TkSignal:

    # create tkshow widget
    tksignal_widget = call_from_tk( create_tksignal_window, *args, **kwargs )
    assert tksignal_widget.is_active

    return tksignal_widget



def create_tksignal_window( *args, **kwargs ) -> TkSignal:
    
    assert threading.current_thread() is tk_main_thread(), "create_tksignal_window has to run within tk thread"
    
    tk_inst = tk_root()

    tk_main = tk.Toplevel(tk_inst)

    # set background black
    tk_main.configure( background='black' )

    #kwargs["borderwidth"] = 0
    #kwargs["highlightthickness"] = 0

    # create tkshow widget
    tksignal_widget = TkSignal( tk_main, *args, **kwargs )
    return tksignal_widget


