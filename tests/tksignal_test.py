# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

import threading
import tkinter as tk
import tkinter.ttk as ttk

from tkshow import show_sequence, TkShowProcess
from tkshow import GraphicsCmdList
from tkshow import tk_init
from tkshow import tk_main_thread

#from PIL import ImageTk
import typing as tp

from PIL import Image
import imageio
import os
from glob import glob
import numpy as np

# import cv2
# import numpy as np


imgDir = "/home/otto/Downloads/datasets/DaimlerBenchmark/SceneLabeling/train_1/imgleft"

img_list = glob(os.path.join( imgDir, '*.png'))
img_list.sort()


def img_loader( idx : int ):
    img = imageio.imread( img_list[idx] )
    img = np.multiply( img, float(255. / img.max()), dtype=np.float32 ).astype(np.uint8)
    #img = Image.fromarray(img)
    #img = Image.open(path)
    #img = cv2.imread( path )
    
    gcl = GraphicsCmdList()
    nidx = idx % 500
    gcl.rectangle( nidx, nidx, nidx+50, nidx+60, outline="#f11", fill="#1f1", alpha=0.50 )
    gcl.zizag_line( 2*nidx, nidx, (2*nidx)+50, nidx+60, (2.3*nidx)+50, (1.2*nidx)+60, fill="cyan", width=3 )
    gcl.polygon( [1.4*nidx, 1.3*nidx, (2.5*nidx)+50, 1.2*nidx+60, (2.3*nidx)+50, (1.2*nidx)+60], outline="white", fill="gray" )
    gcl.oval( (2*nidx)+50, nidx, (1.2*nidx)+60, (2.3*nidx)+50, outline="magenta", width=4 )
    gcl.arc( nidx, nidx, nidx+50, nidx+60, start=30, extent=230, outline="black", width=1 )

    x = np.arange(20,200,20)
    gcl.marker( x=2*x - idx, y=x+idx, symbol="v", size=6, fill="red", alpha=0.3 )
    gcl.marker( x=2*x - idx + 23, y=x+idx + 34, symbol="^", size=5, outline="blue" )
    gcl.marker( x=2*x - idx + 34, y=x+idx + 87, symbol="x", size=5, outline="white" )
    
    return (img,gcl)



def imgIter( imgDir ):
    img_list = glob(os.path.join( imgDir, '*.png'))
    img_list.sort()
    
    while True:
        for idx in range( len(img_list) ):
            img, gcl = img_loader(idx)
            yield img
            yield gcl


def iterator_window():
    show_sequence( imgIter(imgDir))
    print("blub")


def callable_window():
    show_sequence( img_loader, max_idx = len(img_list) - 1, )
    print("blub")


def signal_view_window():
    
    num_frames = len(img_list)

    tkp = TkShowProcess()
    tkp.add_image_view( img_loader, max_idx=num_frames-1 )
    _, tksignal = tkp.add_signal_view( num_axes=2 )
    #tkp.run()
    
    X = np.linspace(0,np.pi*5,num_frames)
    Y1 = np.sin(X)
    Y2 = np.cos(X)

    tksignal.set_time_stamps( X )
    tksignal.set_signal( Y1, ax_idx = 0, label="sin" )
    tksignal.set_signal( Y2, ax_idx = 0, label="cos" )
    tksignal.set_signal( Y1-Y2, ax_idx = 1, label="sin-cos" )
    tksignal.replot()

    tkp.run()
    #while True:
    #    if not tksignal.is_active():
    #        print("tksignal closed")
    #        break
    #
    #    tk_main_thread().join( 0.03 )
    #
    #return tksignal
    # # plot sine
    # X = np.linspace(0,np.pi*5,len(img_list))
    # Y = np.sin(X)

    # tksignal.plot_signal(X,Y)
    # tksignal.set_xpos( X[len(X) >> 2] )

    # print("Signal main thread closed")
    # return tksignal


# ------------------------------------------------------------------------


def main():
    signal_view_window()
    #callable_window()
    #iterator_window()


# ------------------------------------------------------------------------


if __name__ == "__main__":
    tk_init( main_func=main )
