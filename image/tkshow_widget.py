# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["Tkshow", "open_tkshow_window"]


import typing as tp
import tkinter as tk
import queue
import threading
#from PIL import ImageTk

from ..base import tk_quit, call_from_tk, tk_main_thread, tk_root
from .tkshow_base import TkShowBase
from ..base import ControlResponse
from ..base import SimpleQueue, DummyQueue, TKSHOW_EVENT
from ..base import TKSHOW_FEATURE

# ------------------------------------------------------------------------------------------


class Tkshow(TkShowBase):

    __cmd_menu : tk.Menu
    __aspect_ratio_chk_button_value : tk.IntVar
    @property
    def is_preserving_aspect_ratio(self) -> bool:
        return bool(self.__aspect_ratio_chk_button_value.get())



    def __init__(self, master,
                       features : TKSHOW_FEATURE = TKSHOW_FEATURE.NONE, **argv):
        assert isinstance( features, TKSHOW_FEATURE )

        if features & TKSHOW_FEATURE.EXIT:
            argv["close_func"] = self.do_exit
        
        # initalize Tkshow base class
        TkShowBase.__init__(self, master, **argv )

        # create right click menu
        self.__create_menu(features)

        # enable callback for right click
        master.bind("<Button-3>", self.__do_menu )
        master.bind("<m>", self.__do_menu )
        master.bind("<M>", self.__do_menu )


    def __create_menu(self, features : TKSHOW_FEATURE ):

        self.__cmd_menu = tk.Menu(self, tearoff=False)

        self.__cmd_menu.add_command(label = "Close menu", underline=6, command=None)
        
        if features & TKSHOW_FEATURE.SAVE_IMAGE:
            self.__cmd_menu.add_command(label = "Save image", underline=0, accelerator="Ctrl+S", command=self.control_response.do_save_image)
            self.master.bind("<Control-s>", self.do_save_image)

        self.__cmd_menu.add_separator()

        if features & TKSHOW_FEATURE.EXIT:
            self.__cmd_menu.add_command(label = "Exit", underline=1, accelerator="Esc", command=self.do_exit)
        else:
            self.__cmd_menu.add_command(label = "Close window", accelerator="Esc", command=self.do_close)


        self.__cmd_menu.add_separator()

        if features & TKSHOW_FEATURE.FORWARD_START_STOP:
            self.__cmd_menu.add_command(label = "Forward start/stop", accelerator="F5", command = self.control_response.do_forward)
            self.master.bind("<F5>", self.control_response.do_forward)
            self.master.bind("<space>", self.control_response.do_forward)
        
        if features & TKSHOW_FEATURE.BACKWARD_START_STOP:
            self.__cmd_menu.add_command(label = "Backward start/stop", accelerator="F6", command = self.control_response.do_backward)
            self.master.bind("<F6>", self.control_response.do_backward)
        
        if features & TKSHOW_FEATURE.NEXT_IMAGE:
            self.__cmd_menu.add_command(label = "Next image", accelerator="Left", command = self.control_response.next_image)
            self.master.bind("<Right>", self.control_response.next_image)

        if features & TKSHOW_FEATURE.PREV_IMAGE:
            self.__cmd_menu.add_command(label = "Previous image", accelerator="Right", command = self.control_response.previous_image)
            self.master.bind("<Left>", self.control_response.previous_image)
        
        self.__cmd_menu.add_separator()
        
        self.__aspect_ratio_chk_button_value = tk.IntVar(value=1)
        self.__cmd_menu.add_checkbutton(label = "Preserve aspect ratio", underline=9, accelerator="A", command=self.preserve_aspect_ratio, var=self.__aspect_ratio_chk_button_value)
        
        self.__cmd_menu.add_command(label = "Toggle best fit", underline=12, accelerator="F", command=self.toggle_best_fit)
        self.__cmd_menu.add_command(label = "Toggle fullscreen", accelerator="F11", command=self.toggle_fullscreen)
        #self.__cmd_menu.add_separator() 

    
    def __do_menu(self, event):
        try: 
            self.__cmd_menu.tk_popup(event.x_root, event.y_root) 
        finally: 
            self.__cmd_menu.grab_release()


    def do_close(self, event=None):
        if self.control_response.do_close(event):
            super().do_close(event)
    
    def do_exit(self, event=None):
        if self.control_response.do_exist(event):
            super().do_close(event)
            self.after_idle( tk_quit )


    def preserve_aspect_ratio(self, event=None):
        
        if event is not None:
            # called by keyboard
            
            # toggle value of checkbutton
            self.__aspect_ratio_chk_button_value.set( int(not self.is_preserving_aspect_ratio) )

        if self.is_preserving_aspect_ratio:
            super().preserve_aspect_ratio(event)
        

    def on_resize(self, event):
        n_size = (event.width, event.height)

        if self.trg_img_size is not None:
            # Image size has already been defined and might be changed
            n_size = (event.width, event.height)

            if (n_size[0] != self.trg_img_size[0]) or (n_size[1] != self.trg_img_size[1]):
                # position might have been changed as well
                n_pos = (event.x, event.y)
                
                if self.is_preserving_aspect_ratio:
                    n_size = self.calc_aspect_ratio_correction( n_size )
                
                self.set_size( *n_size, pos=n_pos )
                self.master.configure(width=n_size[0], height=n_size[1])

#--------------------------------------------------------------------------------------------------------------

def open_tkshow_window( *args, **kwargs ) -> Tkshow:

    # create tkshow widget
    tkshow_widget = call_from_tk( create_tkshow_window, *args, **kwargs )
    assert tkshow_widget.is_active

    return tkshow_widget



def create_tkshow_window( *args, **kwargs ) -> Tkshow:
    
    assert threading.current_thread() is tk_main_thread(), "create_tkshow_window has to run within tk thread"
    
    tk_inst = tk_root()

    tk_main = tk.Toplevel(tk_inst)

    # set background black
    tk_main.configure( background='black' )

    kwargs["borderwidth"] = 0
    kwargs["highlightthickness"] = 0

    # create tkshow widget
    tkshow_widget = Tkshow( tk_main, *args, **kwargs )
    return tkshow_widget
