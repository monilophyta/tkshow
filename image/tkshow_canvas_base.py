# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["BaseCanvas"]


import typing as tp
import tkinter as tk
import threading

from ..base import TkshowABC
from ..base import ControlResponse

# ------------------------------------------------------------------------------------------

class BaseCanvas(tk.Canvas, TkshowABC):

    @property
    def is_fullscreen(self) -> bool:
        return bool(self.master.attributes('-fullscreen' ))
    
    @property
    def is_maximized(self) -> bool:
        return (self.is_fullscreen or (self.master.state() == "zoomed"))


    def __init__(self, master, 
                       control_response : ControlResponse,
                       close_func = None,
                      **argv):
        tk.Canvas.__init__(self, master, **argv)
        TkshowABC.__init__(self, control_response=control_response)

        if close_func is None:
            close_func = self.do_close

        master.protocol("WM_DELETE_WINDOW", close_func )
        master.bind('<Escape>', close_func )
        
        # trigger activity
        self._set_active()

