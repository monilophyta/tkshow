# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["TkShowBase"]


import sys
import typing as tp
import tkinter as tk
#import threading
from PIL import Image, ImageTk

from .tkshow_canvas_base import BaseCanvas
from .tkshow_drawing import GraphicsCmdList, GCL

# ------------------------------------------------------------------------------------------


class TkShowBase(BaseCanvas):

    __img        : tp.Optional[Image.Image]
    scaled_img   : tp.Optional[Image.Image]

    __trg_img_size : tp.Optional[tp.Tuple[int,int]]

    # store geometry for best fit
    __alternate_geometry : tp.Optional[str]
    
    # graphics command
    __gcl : tp.Optional[GraphicsCmdList] = None
    __gcl_image_storage : list


    @property
    def trg_img_size(self):
        return self.__trg_img_size
    
    def get_xy_scales(self):
        os = self.orig_img_size
        ts = self.trg_img_size
        return (ts[0] / os[0], ts[1] / os[1])
    
    @property
    def orig_img_size(self):
        if self.__img is not None:
            return (self.__img.width, self.__img.height)
        return None
    @property
    def scaled_img_size(self):
        if hasattr( self, "scaled_image" ):
            return (self.scaled_image.width(), self.scaled_image.height())
        return None
    @property
    def canvas_size(self):
        return (self.winfo_width(), self.winfo_height())
    @property
    def window_size(self):
        return (self.master.winfo_width(), self.master.winfo_height())



    def __init__(self, master, img_shape=None, **argv ):
        super().__init__(master, **argv)

        master.bind("<Configure>", self.on_resize)

        if img_shape is None:
            self.__trg_img_size = None
        else:
            assert len(img_shape) == 2
            self.__trg_img_size = img_shape
        
        self.__img = None
        self.__gcl_image_storage = list()

        # key for fullscreen toggling
        master.bind("<F11>", self.toggle_fullscreen)
        
        # key for best possible window scaling
        self.__alternate_geometry = None
        master.bind("<f>", self.toggle_best_fit)
        master.bind("<F>", self.toggle_best_fit)

        # key for toggling between free scaling and aspect ratio preservation
        master.bind("<a>", self.preserve_aspect_ratio)
        master.bind("<A>", self.preserve_aspect_ratio)


    def calc_aspect_ratio_correction(self, img_size) -> tp.Tuple[int,int]:
        orig_wh_scale = float(self.orig_img_size[0]) / float(self.orig_img_size[1])

        img_size1 = ( img_size[0], int( round(img_size[0] / orig_wh_scale) ) )
        img_size2 = ( int(round(img_size[1] * orig_wh_scale)), img_size[1] )

        if (img_size1[0] * img_size1[1]) > (img_size2[0] * img_size2[1]):
            return img_size2
        else:
            return img_size1


    def correct_window_geometry(self, pos : tp.Optional[tp.Tuple[int,int]] = None):
        if not self.is_maximized:
            if pos is not None:
                self.master.geometry("%dx%d+%d+%d" % (self.trg_img_size + pos))
            else:
                self.master.geometry("%dx%d" % self.trg_img_size)


    def set_size(self, width : int, height : int, pos : tp.Optional[tp.Tuple[int,int]] = None ):
        self.__trg_img_size = (width,height)
        self.trigger_task( self.rescale_image )
        self.trigger_task( self.correct_window_geometry, pos )


    def __set_gcl( self, gcl : tp.Optional[GraphicsCmdList] ):
        if isinstance( gcl, GraphicsCmdList ):
            xs,ys = self.get_xy_scales()
            gcl_directives = gcl.get_directives(xs,ys)
            
            for graphics_cmd, gcl_args, gcl_kwargs in gcl_directives:
                if graphics_cmd is GCL.IMAGE:
                    self.__gcl_image_storage.append( gcl_kwargs["image"] )
                    self.create_image( gcl_args, **gcl_kwargs )
                else:
                    getattr( self, "create_" + graphics_cmd.value )( gcl_args, **gcl_kwargs )


    def set_gcl(self, gcl : GraphicsCmdList ):
        self.__gcl = gcl
        self.__set_gcl( gcl )
    

    def __set_image(self, img : ImageTk.PhotoImage ):
        self.scaled_image = img

        img_size = (img.width(), img.height())

        # Using Canvas for image Visualization
        if img_size != self.canvas_size:
            self.configure(width=img_size[0], height=img_size[1])
            self.pack_configure()

        self.delete(tk.ALL)
        self.create_image(0, 0, anchor=tk.NW, image=img)
        

    def set_image(self, orig_img : Image.Image, 
                      scaled_img : ImageTk.PhotoImage,
                             gcl : tp.Optional[GraphicsCmdList] = None ):
        assert isinstance( orig_img, Image.Image )
        assert isinstance( scaled_img, ImageTk.PhotoImage )
        
        scaled_size = (scaled_img.width(), scaled_img.height())

        if self.trg_img_size is None:
            self.__trg_img_size = scaled_size
        
        # store image for potential later rescaling
        self.__img = orig_img

        # resize image if required
        if self.trg_img_size != scaled_size:
            # do not use pre-scaled image
            scaled_img = ImageTk.PhotoImage( orig_img.resize(self.trg_img_size, resample=Image.NEAREST) )
        else:
            # Use prescaled image
            #scaled_img = ImageTk.PhotoImage( scaled_img )
            pass

        # finally set image
        self.__set_image( scaled_img )

        # delete existings graphics commands for last image
        self.__gcl = None
        self.__gcl_image_storage = list()
        
        # draw now, if possible
        if gcl is not None:
            self.set_gcl( gcl )


    def rescale_image(self):
        if self.scaled_img_size is not None:
            if self.trg_img_size != self.scaled_img_size:
                self.__set_image( ImageTk.PhotoImage( self.__img.resize(self.trg_img_size) ) )
                self.__set_gcl( self.__gcl )


    def on_resize(self, event):
        if self.trg_img_size is not None:
            # Image size has already been defined and might be changed
            self.__trg_img_size = (event.width, event.height)
            self.trigger_task( self.rescale_image )
        
        self.master.configure(width=event.width, height=event.height)


    def toggle_fullscreen(self, event=None):
        self.master.attributes('-fullscreen', not self.is_fullscreen )
    

    def toggle_best_fit(self, event=None):
        if self.__img is not None:
            # disable fullscreen if enabled
            self.master.attributes('-fullscreen', False )  
            
            if self.__alternate_geometry is not None:
                # go back to previous geometry
                self.master.geometry(self.__alternate_geometry)
                self.__alternate_geometry = None

                # trigger aspect ratio checking
                self.after_idle( self.preserve_aspect_ratio )
            else:
                # create optimal geometry

                # possible scales along axes
                width_scale = float(self.master.winfo_screenwidth()) / float(self.__img.width)
                height_scale = float(self.master.winfo_screenheight()) / float(self.__img.height)

                # resulting scale
                img_scale = min(width_scale, height_scale)

                # new image sizes
                n_width  = int( img_scale * float(self.__img.width))
                n_height = int( img_scale * float(self.__img.height))

                self.__alternate_geometry = self.master.winfo_geometry()
                self.master.geometry("%dx%d+0+0" % (n_width, n_height))


    def preserve_aspect_ratio(self, event=None):
        corrected_size = self.calc_aspect_ratio_correction(self.trg_img_size)
        self.set_size(*corrected_size)
        


