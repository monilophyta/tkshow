# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__= ["GCL", "GraphicsCmdList"]


import enum
import typing as tp
import functools

import tkinter as tk
from PIL import Image, ImageTk, ImageDraw, ImageColor
import numpy as np


class GCL( enum.Enum ):

    ZIGZAG_LINE = "line"
    POLYGON     = "polygon"
    RECTANGLE   = "rectangle"
    OVAL        = "oval"
    ARC         = "arc"
    IMAGE       = "image"



class GraphicsCmdList( object ):

    directives : list

    def __init__(self):
        self.directives = list()
    
    def get_directives(self, xscale : float, yscale : tp.Optional[float] = None):
        if yscale is None:
            yscale = xscale
        
        directives = [ (gc, args(xscale,yscale), kwargs(xscale,yscale)) for (gc,args,kwargs) in self.directives ]
        return directives


    def __rectangle_like( self, gcl_cmd, coords, kwargs ):
        if "alpha" in kwargs:
            # transparent rectangular patch is created
            alpha = int( round( kwargs.pop("alpha") * 255 ) )

            for atr in ('fill', 'outline'):
                if atr in kwargs:
                    kwargs[atr] = ImageColor.getrgb( kwargs[atr] ) + (alpha,)

            coords = np.array(coords, dtype=np.float32)

            # left upper (NW) edge
            anker = (float(coords[::2].min()), float(coords[1::2].min()))
            
            args_func = functools.partial( lambda xs,ys,anker: (anker[0]*xs,anker[1]*ys), anker=anker )
            kwargs_func = functools.partial( create_transparent_figure, gcl_cmd=gcl_cmd, coords=coords, kwargs=kwargs )

            self.directives.append( (GCL.IMAGE, args_func, kwargs_func) )
        else:
            # simple graphical primitive is stored
            args_func = functools.partial( lambda *s, coords: [ s[idx & 1] * x for idx,x in enumerate(coords) ], coords=coords )
            kwargs_func = functools.partial( lambda xs, ys, kwargs: kwargs, kwargs=kwargs )
            
            self.directives.append( (gcl_cmd, args_func, kwargs_func) )


    def rectangle(self, x1, y1, x2, y2, **kwargs ):
        return self.__rectangle_like( GCL.RECTANGLE, (x1, y1, x2, y2), kwargs )

    def zizag_line(self, x1, y1, x2, y2, *coords, **kwargs ):
        coords = (x1,y1,x2,y2) + coords
        return self.__rectangle_like( GCL.ZIGZAG_LINE, coords, kwargs )

    def polygon(self, coords, **kwargs ):
        assert (len(coords) % 2) == 0
        return self.__rectangle_like( GCL.POLYGON, coords, kwargs )

    def oval( self, x1, y1, x2, y2, **kwargs ):
        return self.__rectangle_like( GCL.OVAL, (x1, y1, x2, y2), kwargs )
    
    def arc( self, x1, y1, x2, y2, start, extent, **kwargs ):
        kwargs["start"] = start
        kwargs["extent"] = extent
        return self.__rectangle_like( GCL.ARC, (x1, y1, x2, y2), kwargs )
    

    def marker(self, x, y, symbol : str, size : int = 4, outline : tp.Optional[str] =None, fill=None, width : int = 1, alpha : float = 1. ):
        if isinstance( x, (int,float) ):
            x = [x]
            y = [y]

        assert len(x) == len(y)
        
        if alpha < 1:
            alpha = int( round( alpha * 255 ) )
            
            if fill is not None:
                fill = ImageColor.getrgb(fill) + (alpha,)
            
            if outline is not None:
                outline = ImageColor.getrgb(outline) + (alpha,)

        marker_patch = create_marker( symbol, size, outline, fill, width )

        # add directives
        marker_params = dict(anchor=tk.CENTER, image=marker_patch)
        kwargs_func = functools.partial( lambda xs, ys, marker_params: marker_params, marker_params=marker_params )

        for X, Y in zip(x,y):
            args_func = functools.partial( lambda xs,ys,X,Y: (xs*X,ys*Y), X=float(X), Y=float(Y) )
            self.directives.append( (GCL.IMAGE, args_func, kwargs_func) )

    
# ----------------------------------------------------------------------------------

@functools.lru_cache(maxsize=256)
def create_marker( marker : str, size : int, outline : str, fill : str, width : int ):
    # drawing according to:
    # https://pillow.readthedocs.io/en/stable/reference/ImageDraw.html
    
    bbox_size = size*2
    patch = Image.new("RGBA", (bbox_size+1,bbox_size+1), (255,255,255,0))
    drawer = ImageDraw.Draw( patch )

    if marker == "o":
        drawer.ellipse([0,0,bbox_size,bbox_size], outline=outline, fill=fill )
    elif marker == 's':
        drawer.rectangle([0,0,bbox_size,bbox_size], outline=outline, fill=fill )
    elif marker == '+':
        drawer.line([size, 0, size, bbox_size], fill=fill or outline, width=width )
        drawer.line([0, size, bbox_size, size], fill=fill or outline, width=width )
    elif marker == 'x':
        drawer.line([0, 0, bbox_size, bbox_size], fill=fill or outline, width=width )
        drawer.line([0, bbox_size, bbox_size, 0], fill=fill or outline, width=width )
    elif marker == 'v':
        drawer.polygon([0,0, size,bbox_size, bbox_size,0], fill=fill, outline=outline)
    elif marker == '^':
        drawer.polygon([0,bbox_size, size,0, bbox_size,bbox_size], fill=fill, outline=outline)
    elif marker == '<':
        drawer.polygon([bbox_size,0, 0,size, bbox_size,bbox_size], fill=fill, outline=outline)
    elif marker == '>':
        drawer.polygon([0,0,  bbox_size,size,  0,bbox_size], fill=fill, outline=outline)
    else:
        raise ValueError( "Unsupported marker '%r'" % marker )
    
    # When ImageTk.PhotoImage is called the mein tk thread should be still running
    return ImageTk.PhotoImage( patch )



def create_transparent_figure( xs : float,
                               ys : float, 
                          gcl_cmd : GCL,
                           coords : np.ndarray,
                           kwargs : dict ):
    
    # apply scales
    coords = coords.copy()
    coords[::2] *= xs
    coords[1::2] *= ys
    
    int_coords = np.round(coords).astype(np.int32)

    # get image patch dimensions
    minX = int_coords[::2].min()
    maxX = int_coords[::2].max()
    minY = int_coords[1::2].min()
    maxY = int_coords[1::2].max()

    # center coords
    int_coords[::2] -= minX
    int_coords[1::2] -= minY

    # create transparent image patch
    patch = Image.new("RGBA", (maxX-minX+1,maxY-minY+1), (255,255,255,0))

    # create drawer
    drawer = ImageDraw.Draw( patch )

    # draw figure
    getattr( drawer, gcl_cmd.value )( list(int_coords), **kwargs )

    # convert to tkinter patch
    patch = ImageTk.PhotoImage( patch )

    return dict(anchor=tk.NW, image=patch) 

