# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["TksignalControl"]

import typing as tp
import tkinter as tk
import collections

import numpy as np

from ..base import SimpleQueue, TKSHOW_EVENT
from ..base import TKSHOW_FEATURE
from ..signal import TkSignal, open_tksignal_window



class TksignalControl(object):

    timestamps     : tp.Optional[np.ndarray]
    signal_values  : tp.Optional[list]

    ax_data        : list


    __tksignal : TkSignal
    @property
    def signals(self) -> TkSignal:
        return self.__tksignal

    @property
    def canvas(self) -> tk.Canvas:
        return self.signals.canvas


    def __init__(self, num_axes : int = 1, *args, **kwargs ):
        # create tkshow widget
        self.timestamps = None
        self.signal_values = [ dict() for idx in range(num_axes) ]
        self.__tksignal = open_tksignal_window( *args, num_axes=num_axes, **kwargs )


    def __del__(self):
        self.close()

    def is_active(self):
        return self.signals.is_active


    def close(self):
        pass
        #self.window.trigger_task( self.window.do_close )

    def set_time_stamps(self, tdata : tp.Union[int,np.ndarray] ):
        if isinstance( tdata, (int,float) ):
            tdata = int(tdata)
            tdata = np.arange( tdata )
        else:
            tdata = np.asarray(tdata)
        self.timestamps = tdata


    def set_signal( self, sdata : collections.Sequence,
                         ax_idx : tp.Optional[int] = None, 
                          label : tp.Optional[str] = None ):
        if ax_idx is None:
            ax_idx = np.array( [len(x) for x in self.signal_values] ).argmin()
        if label is None:
            label = "signal_%2d" % sum(len(x) for x in self.signal_values)
 
        sdata = np.ma.asarray( sdata )
        assert sdata.ndim == 1

        self.signal_values[ax_idx][label] = sdata

        if self.timestamps is None:
            self.timestamps = np.arange(sdata.size)


    def replot(self):
        if (self.timestamps is not None) and self.is_active:
            self.signals.trigger_task( self.signals.plot, self.timestamps, *self.signal_values )
    
    def plot(self):
        self.replot()
        

    def set_xpos(self, xpos=None):
        if xpos is not None:
            self.signals.trigger_task( self.signals.set_xpos, int(xpos) )

    
# ------------------------------------------------------------------------------------

# def create_signal_window( num_axes : int = 1 ) -> TksignalControl:
#     num_axes = int(num_axes)
#     assert num_axes > 0
#     return TksignalControl( num_axes = int(num_axes)  )


