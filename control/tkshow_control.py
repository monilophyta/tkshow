# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["TkshowControl"]

import typing as tp

import numpy as np
from PIL import Image, ImageTk


from ..base import ControlResponse
from ..image import Tkshow, open_tkshow_window
from ..image import GraphicsCmdList


img_types = (Image.Image, np.ndarray)


class TkshowControl(object):

    __tkshow : Tkshow

    @property
    def window(self) -> Tkshow:
        return self.__tkshow


    def __init__(self, **kwargs ):
        # create tkshow widget
        self.__tkshow = open_tkshow_window( **kwargs )

    def __del__(self):
        self.close()
        

    def is_active(self):
        return self.window.is_active


    def close(self):
        self.window.trigger_task( self.window.do_close )


    def set_image( self, img : tp.Union[Image.Image,np.ndarray],
                         gcl : tp.Optional[GraphicsCmdList] = None ):
        assert isinstance( img, img_types )

        # make sure image type fits
        img = as_pil_image( img )
        img_size = (img.width, img.height)

        if (self.window.trg_img_size is not None) and (self.window.trg_img_size != img_size):
            # scale the image already here (not in TK thread)
            if (self.window.trg_img_size[0] < img_size[0]) or (self.window.trg_img_size[1] < img_size[1]):
                resample_method = Image.LANCZOS
            else:
                resample_method = Image.NEAREST
            scaled_img = img.resize(self.window.trg_img_size, resample=resample_method)
        else:
            scaled_img = img
        
        scaled_img = ImageTk.PhotoImage( scaled_img )

        self.window.trigger_task( self.window.set_image, img, scaled_img, gcl )


    def set_gcl(self, gcl : GraphicsCmdList ):
        assert isinstance( gcl, GraphicsCmdList )
        self.window.trigger_task( self.window.set_gcl, gcl )
    
# ------------------------------------------------------------------------------------

#def create_window() -> TkshowControl:
#    return TkshowControl(features = TKSHOW_FEATURE.EXIT)


# ------------------------------------------------------------------------------------

def as_pil_image( img : tp.Union[Image.Image,np.ndarray] ):

    if isinstance( img, Image.Image ):
        return img
    
    if isinstance( img, np.ndarray ):
        if np.issubdtype( img.dtype, np.uint8 ):
            if img.ndim == 2:
                return Image.fromarray(img, mode="L")
            elif img.ndim == 3:
                return Image.fromarray(img, mode="RGB")
            elif img.ndim == 4:
                return Image.fromarray(img, mode="RGBA")
            else:
                raise ValueError( "Unsupported image shape %r" % img.shape)
        raise ValueError( "Unspported image dtype %r" % img.dtype )

    raise TypeError( "Unsupported image type %r" % type(img) )

