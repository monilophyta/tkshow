# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["TKSHOW_FEATURE"]


import enum


class TKSHOW_FEATURE( enum.IntFlag ):

    NONE                = 0

    NEXT_IMAGE          = enum.auto()
    PREV_IMAGE          = enum.auto()

    FORWARD_START_STOP  = enum.auto()
    BACKWARD_START_STOP = enum.auto()

    SAVE_IMAGE          = enum.auto()
    EXIT                = enum.auto()


if __debug__:
    features = list( TKSHOW_FEATURE )
    for idx1 in range(1, len(features)):
        for idx2 in range(idx1):
            assert (features[idx1] & features[idx2]) == 0


