# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["ControlResponse"]


import queue
from .tkshow_events import SimpleQueue
from .tkshow_events import TKSHOW_EVENT


class ControlResponse(object):
    
    # event queue for event emission
    __event_queue : SimpleQueue
    @property
    def event_queue(self):
        return self.__event_queue


    def __init__(self):
        self.__event_queue = SimpleQueue()


    def do_close(self, event=None) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.CLOSE_WINDOW,event), block=False )
        except queue.Full:
            return False
        return True


    def do_close(self, event=None) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.CLOSE_WINDOW,None), block=False )
        except queue.Full:
            return False
        return True
    

    def do_exit(self, event=None) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.EXIT,None), block=False )
        except queue.Full:
            return False
        return True


    def do_save_image(self, event=None) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.SAVE_IMAGE,None), block=False )
        except queue.Full:
            return False
    
    def do_forward(self, event=None) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.FORWARD_START_STOP,None), block=False )
        except queue.Full:
            return False
        return True


    def do_backward(self, event=None) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.BACKWARD_START_STOP,None), block=False )
        except queue.Full:
            return False
        return True


    def do_jump(self, pos : int) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.JUMP_TO,int(pos)), block=False )
        except queue.Full:
            return False
        return True


    def next_image(self, event=None) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.NEXT_IMAGE,None), block=False )
        except queue.Full:
            return False
        return True


    def previous_image(self, event=None) -> bool:
        try:
            self.event_queue.put( (TKSHOW_EVENT.PREVIOUS_IMAGE,None), block=False )
        except queue.Full:
            return False
        return True
    