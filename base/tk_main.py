# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = [ "tk_init", "tk_main_thread", "tk_root", "call_from_tk", "tk_quit"]


import typing as tp
import threading
import tkinter as tk
from .tkshow_events import SimpleQueue

# ---------------------------------------------------------------------------------


def tk_init( main_func : tp.Callable ):
    if threading.current_thread() is not threading.main_thread():
        raise RuntimeError( "init_tk needs to be called from main thread")

    app_thread = TkAppThread.get_instance( main_func=main_func, name="Main application thread" )
    #app_thread.start()
    
    if (not app_thread.is_alive()) and (app_thread.tk is None):
        app_thread.start()

    # finished tk main loop
    if app_thread.is_alive():
        app_thread.join()



def tk_quit():
    with TkAppThread.instance_creation_look:
        if tk_main_thread().is_alive():
            root = tk_root()
            if tk is not None:
                # call Tk.quit as soon as possible
                root.after_idle( root.quit )


# ---------------------------------------------------------------------------------



def tk_thread_task( tk_call, 
                  ret_queue : SimpleQueue,
                       args : tuple,
                     kwargs : dict ):
    # get calling result
    tk_call_res = tk_call( *args, **kwargs )
    
    # return result
    ret_queue.put_nowait( tk_call_res )


def call_from_tk( tk_call, *args, **kwargs ):
    # return queue
    ret_queue = SimpleQueue()
    
    # create actual widget
    tk_root().after_idle( tk_thread_task, tk_call, ret_queue, args, kwargs )

    # wait for result
    return ret_queue.get( timeout=5 )

# ---------------------------------------------------------------------------------


def tk_main_thread() -> threading.Thread:
    return threading.main_thread()

def tk_root() -> tk.Tk:
    return TkAppThread.get_instance().tk


class TkAppThread(threading.Thread):

    instance_creation_look : threading.RLock = threading.RLock()

    __tk : tk.Tk
    __main_func : tp.Callable

    @property
    def tk(self):
        return self.__tk

    @classmethod
    def get_instance(cls, main_func : tp.Optional[tp.Callable] = None, name=None ):
        with cls.instance_creation_look:
            try:
                it = cls.__it__
            except AttributeError:
                if main_func is None:
                    raise ValueError( "callable main function has to be provided")
                
                it = cls.__it__ = cls(main_func=main_func, name=name)
            return it
    

    def __init__(self, main_func : tp.Callable, name=None):
        super(TkAppThread, self).__init__(name=name)

        with TkAppThread.instance_creation_look:
            try:
                if TkAppThread.__it__ is not None:
                    raise RuntimeError( "Second instantiation of singleton")
            except AttributeError:
                pass
            
            self.__tk = None
            self.__main_func = main_func
            #self.daemon = True
    

    def start(self):
        try:
            # create main Tk
            self.__tk = tk.Tk()
        
            # Hide root window
            self.__tk.withdraw()
            #self.__tk.after_idle( self.__tk.withdraw )

            # trigger main thread start
            self.__tk.after_idle( super(TkAppThread, self).start )

            # enter tk mainloop
            self.__tk.mainloop()
            #print( "Window mainloop closed" )
        finally:
            with TkAppThread.instance_creation_look:
                self.__tk = None


    def run(self):
        try:
            # run main function
            self.__main_func()
        finally:
            # quit tk after main thread has finished
            tk_quit()
