# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["TkshowABC"]

import typing as tp
from abc import ABC
import threading

from .tkshow_events import SimpleQueue
from .tkshow_control_response import ControlResponse

# ------------------------------------------------------------------------------------------


class TkshowABC(ABC):

    # event flag indicating if window is active
    __activity : threading.Event
    @property
    def is_active(self) -> bool:
        return self.__activity.is_set()
    
    def _set_active(self):
        self.__activity.set()

    # event queue for event emission
    __control_response : ControlResponse
    @property
    def control_response(self) -> ControlResponse:
        return self.__control_response

    @property
    def event_queue(self) -> SimpleQueue:
        return self.__control_response.event_queue

    # ------------------------------------------------------------

    def __init__(self, control_response : ControlResponse ):
        assert isinstance(control_response, ControlResponse)
        self.__control_response = control_response
        
        # create activity event
        self.__activity = threading.Event()


    def wait_until_active(self, timeout = None):
        self.__activity.wait( timeout )


    def trigger_task(self, task, *task_args):
        if self.is_active:
            self.after_idle( task, *task_args )


    def do_close(self, event=None):
        if self.is_active:
            # make window inactive
            self.__activity.clear()
            self.master.withdraw()
            self.master.destroy()
            #self.master.quit()


