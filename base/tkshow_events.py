# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["TKSHOW_EVENT", "DummyQueue", "SimpleQueue"]

import enum

try:
    from queue import SimpleQueue as spg

    class SimpleQueue(spg):
        def __init__(self, *args, max_size=2, **kwargs):
            super(SimpleQueue, self).__init__(*args,**kwargs)
    
except ImportError:
    from queue import Queue as SimpleQueue



class TKSHOW_EVENT(enum.Enum):

    NONE                = enum.auto()

    CLOSE_WINDOW        = enum.auto()
    SAVE_IMAGE          = enum.auto()

    NEXT_IMAGE          = enum.auto()
    PREVIOUS_IMAGE      = enum.auto()

    FORWARD_START_STOP  = enum.auto()
    BACKWARD_START_STOP = enum.auto()

    JUMP_TO             = enum.auto()

    EXIT                = enum.auto()


class DummyQueue(object):

    @staticmethod
    def put( *args, **argv ):
        pass


